extends Control

signal delete_msg

onready var ui_message : Label = $Layout/Message
onready var ui_delete_dialog : ConfirmationDialog = $ConfirmDelete

func set_message(msg : String) -> void:
	ui_message.text = msg

func _on_Delete_pressed() -> void:
	ui_delete_dialog.dialog_text = "Delete message '" + ui_message.text + "'?"
	ui_delete_dialog.show()

func _on_ConfirmDelete_confirmed() -> void:
	emit_signal("delete_msg")
	queue_free()
