extends Control

const MSGVIEW : PackedScene = preload("res://MessageView.tscn")
const FILE_MESSAGES : String = "RotatingMessages.data"

var skip_message_for_inactive_chat : bool = true
var message_sound : bool = false
var message_queue : Array = []
var msg_delay : int = 300
var chat_active_delay : int = 300

var chat_active : bool = false

func store() -> void:
	var data : Dictionary = {
		"skip_message_for_inactive_chat" : skip_message_for_inactive_chat,
		"message_sound" : message_sound,
		"message_queue" : message_queue,
		"msg_delay" : msg_delay,
		"chat_active_delay" : chat_active_delay
	}
	var save_file : File = File.new()
	save_file.open(FILE_MESSAGES, File.WRITE)
	save_file.store_line(to_json(data))
	save_file.close()

func _ready() -> void:
	# load and apply stored settings
	var save_file = File.new()
	if not save_file.file_exists(FILE_MESSAGES):
		print("No saved data")
		return
	save_file.open(FILE_MESSAGES, File.READ)
	var current_line : Dictionary = parse_json(save_file.get_as_text())
	save_file.close()
	if current_line.has("skip_message_for_inactive_chat"):
		skip_message_for_inactive_chat = (
			current_line["skip_message_for_inactive_chat"])
		$WaitForMessages.pressed = skip_message_for_inactive_chat
	if current_line.has("message_sound"):
		message_sound = current_line["message_sound"]
		$EnableSound.pressed = message_sound
	if current_line.has("message_queue"):
		message_queue = current_line["message_queue"]
		for msg in message_queue:
			create_msg_view(msg)
	if current_line.has("msg_delay"):
		msg_delay = current_line["msg_delay"]
		$MessageDelay/InputDelay.value = msg_delay
	if current_line.has("chat_active_delay"):
		chat_active_delay = current_line["chat_active_delay"]
		$Activity/InputActivityTimeout.value = chat_active_delay
	
	# start sending messages
	$MsgTimer.start(msg_delay)
	
	# init randomizer to avoid always getting the same message sequence
	randomize()
	
	Globals.twicil.connect("message_recieved", self, "_on_messaged")

func _on_NewMessage_text_entered(new_text : String) -> void:
	if not message_queue.has(new_text):
		message_queue.append(new_text)
		store()
		create_msg_view(new_text)
	$NewMessage.clear()

func create_msg_view(new_msg : String) -> void:
	var msg : Control = MSGVIEW.instance()
	$ScrollContainer/Messages.add_child(msg)
	msg.set_message(new_msg)
	msg.connect("delete_msg", self, "_on_delete_msg_request", [new_msg])

func _on_delete_msg_request(msg : String) -> void:
	message_queue.erase(msg)
	store()

func _on_MsgTimer_timeout() -> void:
	if message_queue.empty():
		print("no messages available")
		return
	if skip_message_for_inactive_chat and (not chat_active):
		print("not spamming inactive chat")
		return
	var index : int = randi() % message_queue.size()
	Globals.twicil.send_message(message_queue[index])

func _on_messaged(sender : String, _msg : String, _emote : Array) -> void:
	if Globals.bot_name == sender:
		print("ignoring messages from self")
		return
	chat_active = true
	$ChatActiveTimer.start(chat_active_delay)
	if message_sound:
		$AudioStreamPlayer.play()

func _on_EnableSound_toggled(button_pressed : bool) -> void:
	message_sound = button_pressed
	store()

func _on_InputDelay_value_changed(value) -> void:
	msg_delay = value
	$MsgTimer.start(msg_delay)
	store()

func _on_InputActivityTimeout_value_changed(value) -> void:
	chat_active_delay = value
	$ChatActiveTimer.start(chat_active_delay)
	store()

func _on_ChatActiveTimer_timeout() -> void:
	chat_active = false

func _on_WaitForMessages_toggled(button_pressed) -> void:
	skip_message_for_inactive_chat = button_pressed
	store()
