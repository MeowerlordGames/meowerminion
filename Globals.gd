extends Node

# store TwiCIL instance globally for easier access to API in child scenes
onready var twicil = get_node("TwiCIL")

# store channel name the bot is posting under
# (needed to distinguish viewer messages from bot messages across scenes)
# consider replacing this by signals if easier/more appropriate :)
var bot_name : String = ""
