extends VBoxContainer

const FILE_CONNECTION : String = "Connection.data"

var nick : String = ""
var client_id : String = ""
var oauth : String = "" 
var channel : String = ""

func store() -> void:
	var data : Dictionary = {
		"nick" : nick,
		"client_id" : client_id,
		"oauth" : oauth,
		"channel" : channel,
	}
	var save_file : File = File.new()
	save_file.open(FILE_CONNECTION, File.WRITE)
	save_file.store_line(to_json(data))
	save_file.close()

# loads and applies stored settings
func _ready() -> void:
	var save_file = File.new()
	if not save_file.file_exists(FILE_CONNECTION):
		print("No saved data")
		return
	save_file.open(FILE_CONNECTION, File.READ)
	var current_line : Dictionary = parse_json(save_file.get_as_text())
	save_file.close()
	if current_line.has("nick"):
		nick = current_line["nick"]
		$EditNick.text = nick
	if current_line.has("client_id"):
		client_id = current_line["client_id"]
		$EditClientId.text = client_id
	if current_line.has("oauth"):
		oauth = current_line["oauth"]
		$EditOauth.text = oauth
	if current_line.has("channel"):
		channel = current_line["channel"]
		$EditChannel.text = channel

func _on_Connect_pressed() -> void:
	if nick.empty():
		nick = channel
	Globals.twicil.connect_to_twitch_chat()
	Globals.twicil.connect_to_channel(channel, client_id, oauth, nick)
	Globals.twicil.set_logging(true)
	$Connect.disabled = true
	$Connect.text = "Connection already established!"
	Globals.bot_name = channel

func _on_SendTestMessage_pressed() -> void:
	Globals.twicil.send_message("Test From MeowerMinion")

func _on_EditChannel_text_changed(new_text : String) -> void:
	channel = new_text
	store()

func _on_EditClientId_text_changed(new_text : String) -> void:
	client_id = new_text
	store()

func _on_EditOauth_text_changed(new_text : String) -> void:
	oauth = new_text
	store()

func _on_EditNick_text_changed(new_text : String) -> void:
	nick = new_text
	store()
